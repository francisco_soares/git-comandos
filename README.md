## **Configurando o usuário no Git**

**System:** Para todos do computador.<br>
git config --system user.name **[seu-nome]**<br>
git config --system user.email **[seu-email]**<br><br>

**Local:** Para esse projeto.<br>
git config --local user.name **[seu-nome]**<br>
git config --local user.email **[seu-email]**<br><br>

**Global:** Para todo os projetos do Git.<br>
git config --global user.name **[seu-nome]**<br>
git config --global user.email **[seu-email]**<br>
git config --global core.editor notepade *(Definindo o editor de código)*<br>
git config --global -l *(Lista as configurações feitas)*<br><br>

## **Verificando as configurações gerais**

git config --global -l<br><br>

## **Inserido o editor**

git config --global core.editor **[nome-editor]**<br>
git config --global core.editor notepad<br><br>

**INSERINDO O SUBLIME COMO MEU EDITOR**<br>
git config --global core.editor "'C:/Program Files/Sublime Text 3/sublime_text.exe' -w"<br>
**DÚVIDAS PARA OUTROS EDITORES:** [Link](https://docs.github.com/pt/github/getting-started-with-github/getting-started-with-git/associating-text-editors-with-git)



## **Ignorando arquivos especificos**
**.gitignore** - Crie um arquivo .gitignore na raiz do projeto.<br>
Após criado, abra o mesmo e coloque o tipo de arquivo que quer ignorar no projeto, ex: *.txt (ignora todos arquivos .TXT), *.doc, *.pptx, *.zip e etc. Um abaixo do outro.

## **Comandos básicos**

**git status**  - *Veificar os status dos arquivos*<br><br>
**git add [nome do arquivo] ou [.]** que inseri todo os arquivos modificados  - *Inseri os arquivos na área de transferencia (STAGE AREA)*<br><br>
**git commit -m "mensagem aqui"** - *Comando para rastreamento o -m significa inserir uma mensagem ao commit*<br><br>
**git commit -am "mensagem aqui"** - *Esse comando inseri os arquivos modificados e comita ao mesmo tempo, lembrando que isso se aplica aos arquivos rastreados*<br><br>
**git push** - *Esse comando envia os arquivos para o repositório la no GitHub ou GitLab, depende do repositório que você está usando*<br><br>

**git pull** - *Esse comando atualiza seu repositório local com o repositório no GitHub ou GitLab, depende do repositório que você está usando*<br><br>

**git add [nome-arquivo] && git commit -m "[mensagem-aqui]"** - Módulo simplificado de fazer o ADD e COMMIT.<br><br>


## **Comando para verificar os históricos**

**git log** - *Mostra um histórico básico*<br>
**git checkout [codigo do commit]** - *Mostra os detalhes do commit selecionado me hash*<br>
**git checkout master** - *Volta para o commit atual*
**git log --graph** - *Mostra o log em gráfico*
**git log --graph --oneline** - *Mostra o log em uma linha*<br><br>
**git log --decorate --oneline** - *Mostra o log em uma linha e onde está a HEAD*<br><br>

## **Branch**

**git branch [nome da branch]** - *Cria a branch*<br>
**git checkout [nome da branch]** - *Acessa a branch criada*<br>
**git checkout -b [nome da branch]** - *Cria a nova branch e entra nela*<br>
**git checkout master** - *Volta para a branch master*<br>
**git merge [nome da branch]** - *Junta a branch criada com a Master*<br>
**git branch -d [nome da branch]** - *Exclui a branch*<br>
**git push origin [nome da branch]** - *Isso envia a Branch para o GitHub ou GitLab*<br>
**git branch -v ou git branch** - *Mostra as Branchs criadas*<br>
**git push -d origin [nome da branch]** - *Isso exclui a Branch lá no GitHub ou GitLab*<br>
**git branch -a** - *mostra as branches locais e remotas que temos acesso.*<br>

**O comando abaixo criar uma nova branch e liga ela a branch remota**
```
git checkout --track -b branch1local origin/branch1
```

**Caso já tenha a branch criada**
```
git pull <origin> <apelido_do_repositório_remoto>
```

**FAST-FORWARD MERGE** - *Isso acontece quando realizamos o merge de uma branch sem ter nenhum conflito*<br>
**RECURSIVE STRATEGY** - *Onde consigo mergear o arquivo onde, eu faço um commit na branch que eu criei e crio outro arquivo na master.*<br>
**CONFLITO EM MERGE** - *Acontece quando faço alterações em um mesmo arquivo, tanto na branch criada quanto na master. Após realizar a correção dos conflitos, você deve adicionar a Stage(git add . ou git add <nome-do-arquivo>) e fazer o novo Commit.*<br><br>

## **ISSUES**

> - Issues é traduzido por Problemas.<br>
> - Issues é uma ótima maneira de manter o controle de tarefas, melhorias e bugs para seus projetos. Eles são parecidos com um e-mail, exceto que podem ser compartilhadas e discutidas com o resto de sua equipe.<br>
> - Na ferramenta gráfica de Issues tanto no GitHub quando no GitLab, você consegue colocar o Hash do commmit quando foi comentar a issue realizada. Isso na versão gráfica. Dúvidas, veja a aula: [Aula 27 - Criando e finalizando Issues](https://www.udemy.com/course/dominando-git-e-github/learn/lecture/8112494#questions)<br><br>

**git commit -m "Mensagem aqui #2"** - *Esse #2 serve para mencionar a Issue e comenta-lá automaticamente*<br>
**git commit -m "Mensagem aqui closes #2"** - *Esse close #2 informar a issues que será comentada e finalizada, porém, isso acontece quando mergear e dar um push na master*<br>
**git commit -m "Mensagem aqui fixes #2"** - *Esse fixes #2 informar a issues que será comentada e finalizada, porém, isso acontece quando mergear e dar um psuh na master*<br><br>

**Assinaturas ou Assign:** É utilizada para incluir pessoas especificas para ver/acompanhar a Issues criada. Dúvida, veja a aula [Aula 33 - Assinatura e encerramento da Issues](https://www.udemy.com/course/dominando-git-e-github/learn/lecture/8112606#questions)

> **Labels:** São etiquetas/marcaçoes onde determina o tipo de sua natureza, ex: Parte de um projeto onde temos que desenvolver uma melhoria do sistema a label pode se chamar **MELHORIAS DO SISTEMA**. Dúvidas, veja a aula [Aula 31 - Labels em Issues](https://www.udemy.com/course/dominando-git-e-github/learn/lecture/8112564#questions)<br><br>

> **Milestones:** São agregadores de Issues (Ou agrupadores de issues), onde mostra uma tarefa especifica para fazer e assim que terminada ela é finalizada junto com a issues. Dúvida, veja a aula [Aula 34 - Issues e Milestones](https://www.udemy.com/course/dominando-git-e-github/learn/lecture/8112618#questions) <br><br>

## **DESFAZENDO OPERAÇÕES com Checkout e Reset**

**git checkout [nome do arquivo]** - *Faça isso quando criar uma alteração ou arquivo e quando não usar o git add*<br>
**git reset HEAD [nome do arquivo]** - *Faça isso quando usar o git add, assim você consegue reverter as alterações*<br>
**git reset head^ [nome do arquivo]** - *Faça isso quando usar o commit, o comando head^, informa que vai retornar a um commit anterior*<br>
**git reset --hard [hash-commit]** - *Apaga tudo que aconteceu no commit*<br>
**git rm --cached [nome do arquivo]** - *Informa para o git não rastrear mais o arquivo selecionado*<br><br>


> Dúvidas, veja a aula [Aula 35 - Desfazendo operações com Checkout e Reset](https://www.udemy.com/course/dominando-git-e-github/learn/lecture/8993056#questions)<br><br>

## **FORK**

> - Consigo fazer um clone publico para dentro do meu usuário no Git<br>
> - **UPSTREAM** - Repositório principal <br>
> - **FORK** - Repositório copiado (É feito uma cópia para meu GitHub ou GitLab)<br>
> - **LOCAL** - Repositório Local<br>
> - Dúvidas, veja a aula [Aula 36 - Fork e sincronia de repositórios](https://www.udemy.com/course/dominando-git-e-github/learn/lecture/9014120#questions)<br><br>

**PROCESSO DO FORK**<br>
> 1º - Eu devo dar um FORK no repositório que quero contrinuir<br>
> 2º - Eu devo clonar o repositório que foi FORKADO direto do meu usuário<br>
> 3º - Para sincroniczar os repositório, eu preciso fazer uma cópia do UPSTREAM da forma abaixo:<br>
**git remote add upstream [url-do-upstream]**<br>
**git remote -v** - *Verifica quantos repositórios fazem partes do projeto*<br><br>
> 4º - Depois que fazer um "Clone" na UPSTREAM, voce deve fazer um PULL na mesma para deixar o repositório local atualizado:<br>
**git pull upstream master**<br>
> 5º - Para finalizar, você faz um push no repositorio local, assim deixando o repositório FORK sincronizado<br>
**git push**<br><br>

> **Veja até entender essa aula** -  [Aula 37 - Mãos na Massa com Fork!](https://www.udemy.com/course/dominando-git-e-github/learn/lecture/9036584#questions)<br><br>

**REBASE**<br>
- Conseguimos modificar o histórico do projeto (Project History)<br>
- Alterar as ordens dos commits<br>
- Unir commits<br>
- Modificar mensagem dos commits<br>
> Tudo que tem haver com o histórico do projeto, conseguimos alterar com o REBASE<br>
> O COMMIT não são apenas para usar como Backup, ele serve para identificar o histórico dos nossos projetos<br>
> O MERGE não altera(destrói) a linha do tempo de uma brach!<br>
> **Alerta**: Não use o REBASE em um projeto compartilhado, ele pode afetar o histórico de todo o projeto.<br><br>

**O Rebase é feito da Branch criada para a Branch Master**<br>
**git rebase master**<br>
> Depois que fazer o REBASE, acesse novamente a Branch Master e faça o merge. Logo apois isso, você pode verificar o histórico.<br>
> Para resolver um problema no REBASE, você deve voltar para a branch CRIADA e usar o comando **git rebase master**, após escolher o que deve ficar no código, você deve dar um **git add <nome-arquivo>** e logo após, você deve usar o código:<br><br>
**git rebase --continue**<br>
> Se caso apresente um problema de **hit: waiting for your editor to close the file...**, você deve dar um commit antes.<br><br>

**REBASE INTERATIVO**<br>
- Permite mover, editar, juntar commits. Trabalhar em nosso project history<br>
- **LEMBRE**: Use isso apenas localmente, pois ainda não foi compartilhada com ninguém.<br><br>

**git rebase -i <hash-do-commit>**<br>
- Após isso, será aberto o editor de código que eu defini aqui no git.<br>
- Escolha a opção ex: no PICK altere para R, veja que tem uma legenda abaixo. Salve o arquivo<br>
- O git vai abrir mais um arquivo no seu editor e você pode alterar a descrição do commit.<br>

**IMPORTANTE:**Depois que fizer todo esse processo a sua branch MASTER fique aparecendo assim **master|REBASE**, use o código abaixo.<br>
**git rebase --abort**. Dúvidas veja [AQUI](https://docs.github.com/pt/github/getting-started-with-github/using-git/resolving-merge-conflicts-after-a-git-rebase)<br>
> **Veja até entender essa aula** -  [Aula 42 - E se eu pudesse ... rebase interativo](https://www.udemy.com/course/dominando-git-e-github/learn/lecture/8988720#questions)<br><br>

## **AVISO: PROCESSO DE ROTA DE PROJETO**

> Método de trabalho no GIT, ordens de BRANCHS:<br><br>

1º - **Master**: A **branch master** sempre será a principal. Ela será responsável por subir o projeto para produção<br>
2º - **IA**: A **branch ia** será a branch de teste antes de enviar para a Master. Então, quando terminar de fazer os processos de melhorias ou arrumar algum BUG, você deve MERGEAR sua BRANCH com a BRANC IA.<br>
3º - **NOME-BRANCH**: A **branch nome-branch** será a branch que você estará utilizando para resolver algum bug ou melhorando algo. Como mencionado acima, depois de finalizar, você deve mergea-lá com a BRANCH IA.<br><br>

_Esses são os processos para deixar um projeto 100% funcional e envitarmos retrabalho._<br><br>


## **COLABORAÇÃO COM FORK WORKFLOW**
1º - Fazemos o **Fork**<br>
2º - Fazemos o **git clone** do Fork<br>
3º - Fazemos o **git checkout -b**, **git add** e **git commit**<br>
4º - Fazemos o **git push**<br>
5º - Fazemos o **Pull request(PR)**<br>
6º - Fazemos o **git pull upstream** e **git push**<br>

**Veja até entender esse módulo**: [Aulas](https://www.udemy.com/course/dominando-git-e-github/learn/lecture/9037632#overview)



## **COLABORAÇÃO COM GITHUB WORKFLOW**
Antes de começar. O processo com o Github Workflow, você precisa está cadastrado no projeto.<br>
Assim você não precisa fazer um FORK do projeto.<br>
O dono do repositório precisa acessar **settings > Collaborators** e enviar o convite para você.<br><br>

1º - Fazemos o **git clone**<br>
2º - Fazemos o **git checkout -b**, **git add** e **git commit**<br>
3º - Fazemos o **git push**<br>
4º - Fazemos o **Pull request(PR)**<br>
6º - Fazemos o **git pull upstream**<br><br>

**Pull Request**: As pull requests permitem que você informe outras pessoas sobre as alterações das quais você fez push para um branch em um repositório no GitHub.<br><br>

**Limitando algumas a BRANCH**: Dentro do repositório, acesse: Settings > Branches > Protected Branch > Selecione a Branch Master<br>
Habilito as seguintes seleções: **Protect this branch** | **Require pull request reviews before merging**<br>
Esse caminho pode mudar entre o GitHub e GitLab<br>

**INFORMAÇÃO**: Com essa opções acima habilitadas, quando você criar uma nova branch e tentar subir essa branch para o GitHub ou GitLab, o mesmo vai pedir um Pull Request de aprovação.<br><br>

**DICA SOBRE O GIT**: Quando é criado uma branch local com o mesmo nome da branch remoto, o git entende e faz uma tracking de ambas.<br><br>

**Veja até entender esse módulo**: [Aulas](https://www.udemy.com/course/dominando-git-e-github/learn/lecture/9037632#overview)<br><br>

## **STASH*
**git stash** - Coloca os arquivo em uma área temporária.<br>
**git stash apply** - Volta os arquivos da área temporária. Deixando na lista de stash para possível reutilização posterior<br>
**git stash save "[mensagem]"** - Você coloca o arquivo na área temporário porém, você deixa uma mensagem sobre o que é esse arquivo ou o que estava fazendo aqui. Dessa forma você não fica perdido sobre esse Stash.<br>
**git stash list** - Listas os arquivos que estão no Stash.<br>
**git stash show** - Mostra as alterações que tem haver com o último Stash.<br>
**git stash show --patch** - Mostra o que foi alterado no arquivo que está no Stash.<br>
**git stash list --stat** - Mostra o que foi alterado em todos os arquivos que estão no Stash.<br>
**git stash pop** - Você trás o que está na Stash de volta removendo da pilha.<br>
**git stash --keep-index** - Joga para *stash* apenas os arquivos que não foram para a Stage (*git add .*).<br>
**git stash drop [código-stash] -> stash{0}** - Remove/Exclui da pilha o stash salvo. Se você não passar o Código da Stash, ele vai excluir o primeiro da pilha.<br>
**git stash -u** - O Stash funciona apenas com os arquivos rastreados, mas com esse comando, você consegue inserir até os arquivos que não foram rastreados. <br><br>
<br><br>

O *STASH* funciona assim.<br>
Você está trabalhando em uma parte do projeto, e você envia para a stage com o *git add .*, porém, você precisa parar imediatamente o que está fazendo para ir resolver um problema. Você pode usar o comando *Stash* para deixar o que está na *Stage* guardado em uma area temporária até finalizar outro processo prioritário. Terminado o processo, você pode voltar com os códigos salvo na *Stash*.

<br><br>

**DICA PARA RESOLVER CONFLITOS NO STASH:** Você vai se deparar com algum conflito em seu Stash.<br>
Para resolver, você pode fazer o seguinte. Assim que tentar usar o comando *git stash apply* ele vai apresentar um problema.<br>
Você deve dar um commit e depois poderá usar o *git stash apply*. Ele vai mostrar o conflito, você resolve e pode commitar e seguir adiante. Dúvidas, veja esse vídeo: [Aula 55 - Resolvendo conflito com Stash](https://www.udemy.com/course/dominando-git-e-github/learn/lecture/9094208#questions)

<br><br>

**DICA STASH ANTES DE PULL:** - Imagina que você está trabalhando em um arquivo e uma outra pessoa também trabalha nesse mesmo arquivo.<br>
Quando você der um *pull*, vai apresentar um erro pois você está alterando o mesmo arquivo.<br>
Sendo assim, você pode jogar suas alterações na *Stash*, fazer o *pull* e depois trazer os arquivos que estão na *Stash*.<br>
Isso vai gerar um conflito simples e você pode resolver rápido.

<br><br>

## **REPOSITÓRIO LOCAL CONECTANDO AO REPOSITÓRIO REMOTO**

**Imagine a situação**: Você criou uma pasta e deu git init, você está começando um repositório local.<br>
Depois de ter feito todos os processo de git add e git commit, você precisa fazer um push para o repositório remoto no GitHub ou GitLab.<br>
Simples, você pode usar o comando abaixo:<br><br>

**git remote add origin url-do-repositorio-remoto**<br><br>

Assim o repositório remoto ficará ligado ao repositório local.<br>
Porém, sempre que for enviar, eu preciso informar a origin, veja abaixo:<br><br>

**git push origin master**<br><br>

Para contornar isso, eu posso fazer da forma abaixo:

**git push -u origin master** - A Flag *-u*, informa que será enviada para a UpStream, dessa forma eu não preciso informar mais a origin e sim deixar da forma *git push master*.
<br><br>



[Parei aqui](https://www.udemy.com/course/dominando-git-e-github/learn/lecture/8988720#questions)<br><br>



## **ALGO QUE APRENDI NA DUO - RESETAR E DEIXAR O ARQUIVO IGUAL A MASTER**

```
git checkout origin/master -- <nome-doa-rquivo>
git checkout origin/master -- conexao/conexao.php
git checkout origin/master -- index.php
git checkout origin/master -- adm/conexao/conexao.php
```
****
